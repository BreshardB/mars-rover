package tyler_technologies.interview.MarsRover.Controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import tyler_technologies.interview.MarsRover.Exception.InvalidInputException;
import tyler_technologies.interview.MarsRover.Service.MarsRoverService;

@RestController
public class MarsRoverController {

    private final MarsRoverService service;

    public MarsRoverController() {
        service = new MarsRoverService();
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/rovers",
            produces = {"application/json"})
    public String getRovers(@RequestParam String input) {
        try {
            service.parseInput(input);
        } catch (InvalidInputException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
        return service.getResult().toString();
    }

}
