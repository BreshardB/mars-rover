package tyler_technologies.interview.MarsRover.Model;

public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST
}
