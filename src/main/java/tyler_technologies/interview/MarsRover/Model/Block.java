package tyler_technologies.interview.MarsRover.Model;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;

public class Block {

    @Getter
    private final Coordinates coordinates;

    @Getter
    @Setter
    private Object element;

    @Getter
    @Setter
    private HashMap<Direction, Block> surroundings;

    public Block(Coordinates coordinates) {
        this.coordinates = coordinates;
        this.element = null;
        this.surroundings = new HashMap<>();

        this.surroundings.put(Direction.NORTH, null);
        this.surroundings.put(Direction.EAST, null);
        this.surroundings.put(Direction.SOUTH, null);
        this.surroundings.put(Direction.WEST, null);
    }

    // Refers to whether or not a block is occupied by a rover at these coordinates
    public boolean isEmpty() {
        return element == null;
    }

    public Block getSurroundingBlock(Direction direction) {
        return this.surroundings.get(direction);
    }

    public void setSurroundingBlock(Direction direction, Block block) {
        this.surroundings.put(direction, block);
    }
}
