package tyler_technologies.interview.MarsRover.Model;

public class Compass {

    static class Node {
        Direction direction;
        Node left;
        Node right;
    }

    private Node currentDirection;

    public Compass() {
        this(Direction.NORTH);
    }

    public Compass(Direction dir) {

        Node north = new Node();
        north.direction = Direction.NORTH;
        Node west = new Node();
        west.direction = Direction.WEST;
        Node south = new Node();
        south.direction = Direction.SOUTH;
        Node east = new Node();
        east.direction = Direction.EAST;

        // Setting up relative directions for each cardinal direction
        // Uses a circularly linked list
        north.left = west;
        north.right = east;
        west.left = south;
        west.right = north;
        south.left = east;
        south.right = west;
        east.left = north;
        east.right = south;

        // Setting current direction to whatever is provided
        // Default: North
        this.currentDirection = north;
        while(this.currentDirection.direction != dir) {
            this.currentDirection = this.currentDirection.right;
        }
    }

    public Direction getCurrentDirection() {
        return this.currentDirection.direction;
    }

    // Turns the compass clockwise until the desired direction is set
    public void setCurrentDirection(Direction direction) {
        while (this.currentDirection.direction != direction) {
            right();
        }
    }

    public void left() {
        this.currentDirection = this.currentDirection.left;
    }

    public void right() {
        this.currentDirection = this.currentDirection.right;
    }
}
