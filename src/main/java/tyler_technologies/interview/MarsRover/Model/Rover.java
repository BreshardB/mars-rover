package tyler_technologies.interview.MarsRover.Model;

import lombok.Getter;
import lombok.Setter;

public class Rover {

    @Getter
    @Setter
    private Coordinates coordinates;
    private final Compass compass;

    public Rover(int xPosition, int yPosition, Direction direction) {
        this.coordinates = new Coordinates(xPosition, yPosition);
        this.compass = new Compass();
        this.setDirection(direction);
    }

    public Rover(int xPosition, int yPosition, String direction) {
        this.coordinates = new Coordinates(xPosition, yPosition);
        this.compass = new Compass();

        switch(direction) {
            case "n":
            case "N":
                this.setDirection(Direction.NORTH);
                break;
            case "e":
            case "E":
                this.setDirection(Direction.EAST);
                break;
            case "s":
            case "S":
                this.setDirection(Direction.SOUTH);
                break;
            case "w":
            case "W":
                this.setDirection(Direction.WEST);
                break;
            default:
                break;
        }
    }

    public int getXPosition() {
        return this.coordinates.getXAxis();
    }

    public int getYPosition() {
        return this.coordinates.getYAxis();
    }

    public void setXPosition(int xPosition) {
        this.coordinates.setXAxis(xPosition);
    }

    public void setYPosition(int yPosition) {
        this.coordinates.setYAxis(yPosition);
    }

    public Direction getDirection() {
        return this.compass.getCurrentDirection();
    }

    public String getDirectionString() {
        switch (this.compass.getCurrentDirection()) {
            case NORTH:
                return "N";
            case EAST:
                return "E";
            case WEST:
                return "W";
            case SOUTH:
                return "S";
            default:
                break;
        }
        return null;
    }

    public void setDirection(Direction direction) {
        this.compass.setCurrentDirection(direction);
    }

    public void left() {
        this.compass.left();
    }

    public void right() {
        this.compass.right();
    }
}
