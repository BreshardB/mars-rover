package tyler_technologies.interview.MarsRover.Model;

import lombok.Getter;

import java.util.HashMap;

public class Plateau {

    @Getter
    private final int xLength;
    @Getter
    private final int yLength;
    @Getter
    private HashMap<String, Block> record;

    public Plateau(int xLength, int yLength) {
        this.xLength = xLength;
        this.yLength = yLength;
        this.record = new HashMap<>();

        constructPlateau();
    }

    // Puts the newly constructed rover at a block with the provided coordinates
    public void placeRover(Rover rover) {
        this.record
                .get(rover
                        .getCoordinates()
                        .toString())
                .setElement(rover);
    }

    public void move(Rover rover) {
        String coordinate = rover.getCoordinates().toString();
        Direction direction = rover.getDirection();
        Block currBlock = this.record.get(coordinate);
        Block nextBlock = currBlock.getSurroundingBlock(direction);
        if(nextBlock != null && nextBlock.isEmpty()) {
            nextBlock.setElement(rover);
            rover.setCoordinates(nextBlock.getCoordinates());
            currBlock.setElement(null);
        }
    }

    // Recursively build plateau
    private void constructPlateau() {

        Coordinates startingCoord = new Coordinates(0, 0);
        Block startingBlock = new Block(startingCoord);
        record.put(startingCoord.toString(), startingBlock);

        startingBlock.setSurroundingBlock(Direction.NORTH, addAdjacentBlock(0, 1));
        startingBlock.setSurroundingBlock(Direction.EAST, addAdjacentBlock(1, 0));
        startingBlock.setSurroundingBlock(Direction.SOUTH, addAdjacentBlock(0, -1));
        startingBlock.setSurroundingBlock(Direction.WEST, addAdjacentBlock(-1, 0));
    }

    // Recursive method used for plateau construction;
    // Adds neighboring blocks to each constructed block in each cardinal direction
    // within the bounds of the grid/plateau
    private Block addAdjacentBlock(int x, int y) {
        // Base case: when the next block would be put out of the bounds of the grid
        if(x == -1 || x > this.xLength || y == -1 || y > this.yLength) {
            return null;
        }
        Block adjacentBlock;

        Coordinates adjacentCoord = new Coordinates(x, y);
        String adjacentCoordString = adjacentCoord.toString();

        // If there's already a block at the coordinates,
        // simply set it as adjacent to another one
        // without creating a new one; else keep going
        if (record.get(adjacentCoordString) != null) {
            return record.get(adjacentCoordString);
        } else {
            adjacentBlock = new Block(adjacentCoord);
            record.put(adjacentCoordString, adjacentBlock);

            adjacentBlock.setSurroundingBlock(Direction.NORTH, addAdjacentBlock(x, y+1));
            adjacentBlock.setSurroundingBlock(Direction.EAST, addAdjacentBlock(x+1, y));
            adjacentBlock.setSurroundingBlock(Direction.SOUTH, addAdjacentBlock(x, y-1));
            adjacentBlock.setSurroundingBlock(Direction.WEST, addAdjacentBlock(x-1, y));
        }
        return adjacentBlock;
    }
}
