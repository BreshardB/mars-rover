package tyler_technologies.interview.MarsRover.Model;

import lombok.Getter;
import lombok.Setter;

public class Coordinates {

    @Getter
    @Setter
    private int xAxis;
    @Getter
    @Setter
    private int yAxis;

    public Coordinates(int xAxis, int yAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public String toString() {
        return String.format("{x: %d, y: %d}", this.xAxis, this.yAxis);
    }
}
