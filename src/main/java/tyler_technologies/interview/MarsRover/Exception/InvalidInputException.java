package tyler_technologies.interview.MarsRover.Exception;

// Represents a number of possible exceptions that can result from
// arbitrarly bad user input
public class InvalidInputException extends Exception {
    public InvalidInputException(String error) {
        super(error);
    }
}
