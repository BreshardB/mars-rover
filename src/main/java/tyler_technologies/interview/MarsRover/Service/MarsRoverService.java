package tyler_technologies.interview.MarsRover.Service;

import lombok.Getter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import tyler_technologies.interview.MarsRover.Exception.InvalidInputException;
import tyler_technologies.interview.MarsRover.Model.Plateau;
import tyler_technologies.interview.MarsRover.Model.Rover;

@Service
public class MarsRoverService {

    @Getter
    private Plateau plateau = null;

    @Getter
    private JSONObject result;

    // Parses through the input given, calls private methods to create the plateau/grid and rovers and
    // execute the movement commands
    public void parseInput(String input) throws InvalidInputException {
        String [] parse = input.split("\n");

        constructPlateau(parse[0]);

        result = new JSONObject(); // Json to be returned by the resource response
        result.put("rovers", new JSONArray());

        for(int i = 1; i < parse.length; i += 2) {
            Rover rover = constructRover(parse[i]);
            plateau.placeRover(rover);
            followMovementInstructions(rover, parse[i+1]);

            JSONObject resultRover = new JSONObject();
            resultRover.put("coordinates", String.format("%d %d", rover.getXPosition(), rover.getYPosition()));
            resultRover.put("direction", rover.getDirectionString());
            result.getJSONArray("rovers").put(resultRover);
        }
    }

    private void constructPlateau(String input) throws InvalidInputException {
        String trim = input.trim();

        // Making sure grid creation input is valid
        if (!trim.matches("^\\d+[ ]\\d+$"))
            throw new InvalidInputException("First line of input must only contain non-negative integers.");

        String [] dimensions = trim.split(" ");

        plateau = new Plateau(Integer.parseInt(dimensions[0]), Integer.parseInt(dimensions[1]));
    }

    private Rover constructRover(String input) throws InvalidInputException {
        String trim = input.trim();

        // Making sure rover placement has valid input
        if (!trim.matches("^\\d+[ ]\\d+[ ][newsNEWS]$"))
            throw new InvalidInputException("Input for rover placement must only contain two non-negative integers"+
                    " and a direction represented by either N, E, W, or S.");

        String [] values = trim.split(" ");
        int xAxis = Integer.parseInt(values[0]);
        int yAxis = Integer.parseInt(values[1]);
        String direction = values[2];

        // Making sure rover is placed within bounds and not on top of another rover
        if (!(xAxis <= plateau.getXLength() && yAxis <= plateau.getYLength())
                || !this.getPlateau().getRecord().get(String.format("{x: %d, y: %d}", xAxis, yAxis)).isEmpty())
            throw new InvalidInputException("Input for rover placement must be within grid bounds"+
                    " and not in the place of another already-positioned rover.");

        return new Rover(
                xAxis,
                yAxis,
                direction);
    }

    private void followMovementInstructions(Rover rover, String input) throws InvalidInputException {
        if(input == null)
            return;

        String trim = input.trim();

        // Making sure rover movement has valid input
        if(!trim.matches("^[lrmLRM]+$"))
            throw new InvalidInputException("Input for rover movement must only contain alphabetic characters"+
                    " L, R, and M.");

        for(char instruction : trim.toCharArray()) {
            switch(String.valueOf(instruction)) {
                case "l":
                case "L":
                    rover.left();
                    break;
                case "r":
                case "R":
                    rover.right();
                    break;
                case "m":
                case "M":
                    plateau.move(rover);
                    break;
                default:
                    break;
            }
        }
    }
}
