package tyler_technologies.interview.MarsRover.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BlockTest {

    Block block;

    @BeforeEach
    public void setUp() {
        block = new Block(new Coordinates(1, 2));
    }

    @Test
    public void getCoordinatesShouldReturnCorrectCoordinates() {
        assertEquals(block.getCoordinates().getXAxis(), 1);
        assertEquals(block.getCoordinates().getYAxis(), 2);
    }

    @Test
    public void isEmptyShouldReturnTrueForNewBlock() {
        assertTrue(block.isEmpty());
    }

    @Test
    public void getElementShouldReturnElementInBlock() {
        Rover rover = new Rover(0, 0, Direction.NORTH);
        block.setElement(rover);
        assertEquals(block.getElement(), rover);
    }

    @Test
    public void getSurroundingBlockShouldReturnCorrectValue() {
        Block testBlock = new Block(new Coordinates(0, 0));

        block.setSurroundingBlock(Direction.NORTH, testBlock);
        assertEquals(block.getSurroundingBlock(Direction.NORTH), testBlock);

        block.setSurroundingBlock(Direction.EAST, testBlock);
        assertEquals(block.getSurroundingBlock(Direction.EAST), testBlock);

        block.setSurroundingBlock(Direction.SOUTH, testBlock);
        assertEquals(block.getSurroundingBlock(Direction.SOUTH), testBlock);

        block.setSurroundingBlock(Direction.WEST, testBlock);
        assertEquals(block.getSurroundingBlock(Direction.WEST), testBlock);
    }
}