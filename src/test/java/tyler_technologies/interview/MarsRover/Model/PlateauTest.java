package tyler_technologies.interview.MarsRover.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlateauTest {

    Plateau plateau;

    @BeforeEach
    public void setup() {
        plateau = new Plateau(5, 5);
    }

    @Test
    public void constructPlateauShouldHaveAdequateNavigation() {
        assertNotNull(plateau.getRecord());
        assertEquals(plateau.getRecord().size(), (plateau.getXLength()+1)*(plateau.getYLength()+1));
    }

    @Test
    public void placeObjectShouldPlaceRoverInCorrectStartingPosition() {
        Rover rover = new Rover(0, 0, Direction.NORTH);
        plateau.placeRover(rover);
        assertEquals(plateau.getRecord().get("{x: 0, y: 0}").getElement(), rover);
    }

    @Test
    public void moveShouldPlaceRoverAtNextBlockInDirectionFaced() {
        Rover rover = new Rover(0, 0, Direction.NORTH);
        plateau.placeRover(rover);

        plateau.move(rover);
        assertEquals(rover.getXPosition(), 0);
        assertEquals(rover.getYPosition(), 1);
        assertEquals(plateau.getRecord().get("{x: 0, y: 1}").getElement(), rover);
        assertNull(plateau.getRecord().get("{x: 0, y: 0}").getElement());

        rover.right();
        plateau.move(rover);
        assertEquals(rover.getXPosition(), 1);
        assertEquals(rover.getYPosition(), 1);
        assertEquals(plateau.getRecord().get("{x: 1, y: 1}").getElement(), rover);
        assertNull(plateau.getRecord().get("{x: 0, y: 1}").getElement());

        rover.left();
        plateau.move(rover);
        assertEquals(rover.getXPosition(), 1);
        assertEquals(rover.getYPosition(), 2);
        assertEquals(plateau.getRecord().get("{x: 1, y: 2}").getElement(), rover);
        assertNull(plateau.getRecord().get("{x: 1, y: 1}").getElement());
    }
}
