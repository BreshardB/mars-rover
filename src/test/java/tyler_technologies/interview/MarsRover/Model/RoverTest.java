package tyler_technologies.interview.MarsRover.Model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoverTest {

    Rover rover;

    @BeforeEach
    public void setup() {
        rover = new Rover(1, 2, Direction.NORTH);
    }

    @Test
    public void getAndSetXPositionShouldReturnCorrectValue() {
        rover.setXPosition(3);
        assertEquals(rover.getXPosition(), 3);
    }

    @Test
    public void getAndGetYPositionShouldReturnCorrectValue() {
        rover.setYPosition(4);
        assertEquals(rover.getYPosition(), 4);
    }

    @Test
    public void setAndGetDirectionShouldReturnCorrectDirections() {
        rover.setDirection(Direction.SOUTH);
        assertEquals(rover.getDirection(), Direction.SOUTH);
        rover.setDirection(Direction.NORTH);
        assertEquals(rover.getDirection(), Direction.NORTH);
        rover.setDirection(Direction.EAST);
        assertEquals(rover.getDirection(), Direction.EAST);
        rover.setDirection(Direction.WEST);
        assertEquals(rover.getDirection(), Direction.WEST);
    }

    @Test
    public void leftShouldTurnRoverToCorrectDirection() {
        rover.left();
        assertEquals(rover.getDirection(), Direction.WEST);
        rover.left();
        assertEquals(rover.getDirection(), Direction.SOUTH);
        rover.left();
        assertEquals(rover.getDirection(), Direction.EAST);
        rover.left();
        assertEquals(rover.getDirection(), Direction.NORTH);
    }

    @Test
    public void rightShouldTurnRoverToCorrectDirection() {
        rover.right();
        assertEquals(rover.getDirection(), Direction.EAST);
        rover.right();
        assertEquals(rover.getDirection(), Direction.SOUTH);
        rover.right();
        assertEquals(rover.getDirection(), Direction.WEST);
        rover.right();
        assertEquals(rover.getDirection(), Direction.NORTH);
    }
}
