package tyler_technologies.interview.MarsRover.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoordinatesTest {

    Coordinates coordinates;

    @BeforeEach
    public void setUp() {
        coordinates = new Coordinates(1, 2);
    }

    @Test
    public void getXAxisShouldReturnXValue() {
        coordinates.setXAxis(3);
        assertEquals(coordinates.getXAxis(), 3);
    }

    @Test
    public void getYAxisShouldReturnYValue() {
        coordinates.setYAxis(4);
        assertEquals(coordinates.getYAxis(), 4);
    }
}