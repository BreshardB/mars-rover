package tyler_technologies.interview.MarsRover.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CompassTest {

    Compass compass;

    @BeforeEach
    public void setup() {
        compass = new Compass();
    }

    @Test
    public void getCurrentDirectionShouldReturnNorth() {
        compass.setCurrentDirection(Direction.SOUTH);
        assertEquals(compass.getCurrentDirection(), Direction.SOUTH);
        compass.setCurrentDirection(Direction.NORTH);
        assertEquals(compass.getCurrentDirection(), Direction.NORTH);
        compass.setCurrentDirection(Direction.EAST);
        assertEquals(compass.getCurrentDirection(), Direction.EAST);
        compass.setCurrentDirection(Direction.WEST);
        assertEquals(compass.getCurrentDirection(), Direction.WEST);
    }

    @Test
    public void leftShouldMoveCompassToAppropriateDirection() {
        assertEquals(compass.getCurrentDirection(), Direction.NORTH);
        compass.left();
        assertEquals(compass.getCurrentDirection(), Direction.WEST);
        compass.left();
        assertEquals(compass.getCurrentDirection(), Direction.SOUTH);
        compass.left();
        assertEquals(compass.getCurrentDirection(), Direction.EAST);
        compass.left();
        assertEquals(compass.getCurrentDirection(), Direction.NORTH);
    }

    @Test
    public void rightShouldMoveCompassToAppropriateDirection() {
        assertEquals(compass.getCurrentDirection(), Direction.NORTH);
        compass.right();
        assertEquals(compass.getCurrentDirection(), Direction.EAST);
        compass.right();
        assertEquals(compass.getCurrentDirection(), Direction.SOUTH);
        compass.right();
        assertEquals(compass.getCurrentDirection(), Direction.WEST);
        compass.right();
        assertEquals(compass.getCurrentDirection(), Direction.NORTH);
    }
}
