package tyler_technologies.interview.MarsRover.Service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tyler_technologies.interview.MarsRover.Exception.InvalidInputException;

import static org.junit.jupiter.api.Assertions.*;

class MarsRoverServiceTest {

    private MarsRoverService service;

    @BeforeEach
    public void setUp() {
        service = new MarsRoverService();
    }

    @Test
    public void parseInputShouldThrowExceptionWithIncorrectInputForGrid() {
        Exception thrown;

        String alphaInput = "A 5\n";

        thrown = assertThrows(InvalidInputException.class, () -> service.parseInput(alphaInput), "");

        assertTrue(thrown.getMessage().toLowerCase().contains("non-negative integers"));

        String negativeInput = "-5 5\n";

        thrown = assertThrows(InvalidInputException.class, () -> service.parseInput(negativeInput), "");

        assertTrue(thrown.getMessage().toLowerCase().contains("non-negative integers"));
    }

    @Test
    public void parseInputShouldThrowExceptionsWithIncorrectInputForRoverPlacement() {
        Exception thrown;

        String alphaInput = "5 5\n" +
                "1 T N\n" +
                "LMLMLMLMM\n";

        thrown = assertThrows(InvalidInputException.class, () -> service.parseInput(alphaInput), "");

        assertTrue(thrown.getMessage().toLowerCase().contains("non-negative integers"));

        String negativeInput = "5 5\n" +
                "-1 2 N\n" +
                "LMLMLMLMM\n";

        thrown = assertThrows(InvalidInputException.class, () -> service.parseInput(negativeInput), "");

        assertTrue(thrown.getMessage().toLowerCase().contains("non-negative integers"));

        String boundsInput = "5 5\n" +
                "6 5 N\n" +
                "LMLMLMLMM\n";

        thrown = assertThrows(InvalidInputException.class, () -> service.parseInput(boundsInput), "");

        assertTrue(thrown.getMessage().toLowerCase().contains("within grid bounds"));
    }

    @Test
    public void parseInputShouldThrowExceptionWithIncorrectInputForRoverMovement() {
        Exception thrown;

        String movementInput = "5 5\n" +
                "5 5 N\n" +
                "LMLMLMLMMT\n";

        thrown = assertThrows(InvalidInputException.class, () -> service.parseInput(movementInput), "");

        assertTrue(thrown.getMessage().toLowerCase().contains("alphabetic characters"));
    }

    @Test
    public void roverMovementShouldPlaceRoverAtAppropriateSpot() throws InvalidInputException {
        String fullInputSingle = "5 5\n" +
                "1 2 N\n" +
                "LMLMLMLMM";

        service.parseInput(fullInputSingle);
        assertNotNull(service.getPlateau().getRecord().get("{x: 1, y: 3}").getElement());

        String fullInputMulti = "5 5\n" +
                "1 2 N\n" +
                "LMLMLMLMM\n" +
                "3 3 E\n" +
                "MMRMMRMRRM";

        service.parseInput(fullInputMulti);
        assertNotNull(service.getPlateau().getRecord().get("{x: 1, y: 3}").getElement());
        assertNotNull(service.getPlateau().getRecord().get("{x: 5, y: 1}").getElement());
    }

    @Test
    public void parseInputShouldThrowExceptionIfRoverPlacedInOccupiedSpot() {
        Exception thrown;

        String movementInput = "5 5\n" +
                "1 2 N\n" +
                "LMLMLMLMM\n" +
                "1 3 N\n" +
                "LMLMLMLMM";

        thrown = assertThrows(InvalidInputException.class, () -> service.parseInput(movementInput), "");

        assertTrue(thrown.getMessage().toLowerCase().contains("already"));
    }

    @Test
    public void getResultShouldReturnResultSet() throws InvalidInputException {
        String fullInputMulti = "5 5\n" +
                "1 2 N\n" +
                "LMLMLMLMM\n" +
                "3 3 E\n" +
                "MMRMMRMRRM";

        service.parseInput(fullInputMulti);

        assertNotNull(service.getResult());
        assertNotNull(service.getResult().get("rovers"));
        assertEquals(service.getResult().getJSONArray("rovers").length(), 2);
    }

    @Test
    public void getResultShouldNotStopRoverFromCrashingIntoOtherRover() throws InvalidInputException {
        String fullInputMulti = "5 5\n" +
                "1 2 N\n" +
                "LMLMLMLMM\n" +
                "1 2 N\n" +
                "LMLMLMLMM";

        service.parseInput(fullInputMulti);

        assertNotNull(service.getResult());
        assertNotNull(service.getResult().get("rovers"));
        assertEquals(service.getResult().getJSONArray("rovers").length(), 2);
        assertFalse(service.getPlateau().getRecord().get("{x: 1, y: 3}").isEmpty());
        assertFalse(service.getPlateau().getRecord().get("{x: 1, y: 2}").isEmpty());
    }
}