package tyler_technologies.interview.MarsRover.Controller;

import net.minidev.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tyler_technologies.interview.MarsRover.Service.MarsRoverService;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MarsRoverController.class)
class MarsRoverControllerTest {

    private MarsRoverService service;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        service = mock(MarsRoverService.class);
    }

    @Test
    public void getResultShouldReturnResultSetWithStatus200() throws Exception {
        JSONObject rovers = new JSONObject();
        rovers.put("rovers", new JSONArray());

        String input = "5 5\n" +
                "1 2 N\n" +
                "LMLMLMLMM\n" +
                "3 3 E\n" +
                "MMRMMRMRRM";

        doNothing().when(service).parseInput(anyString());
        when(service.getResult()).thenReturn(rovers);

        mockMvc.perform(
                get("/rovers")
                        .param("input", input)
                ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.rovers").exists());
    }

    @Test
    public void getResultShouldThrowExceptionWithStatus400() throws Exception {
        JSONObject rovers = new JSONObject();
        rovers.put("rovers", new JSONArray());

        doNothing().when(service).parseInput(anyString());
        when(service.getResult()).thenReturn(rovers);

        mockMvc.perform(
                get("/rovers")
                        .param("input", "")
                ).andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(ex -> assertTrue(Objects.requireNonNull
                        (ex.getResolvedException()).getMessage().toLowerCase().contains("input")));
    }
}