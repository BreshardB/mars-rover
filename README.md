# Mars Rover

This is my submission for the coding challenge for interview with Tyler Technologies (Socrata). The project that I've chosen is the Mars Rover.

## Project Information

This application is built as a Spring Boot project web service through test-driven development. The unit tests are built using JUnit 5 with some Jacoco for code coverage and Gradle is used as the build tool. There were also some integration tests done through Postman and all were committed into a Gitlab repository which also had some CI/CD pipeline instructions. Some added details are that Lombok was used along with some Swagger UI (though the latter wasn't used too heavily).


## Running the code

As mentioned before, this project was built using Gradle. So building, testing, and executing the application will use Gradle for that purpose.

### Build

```
./gradlew build
```

### Test

```
./gradlew test
```
Note: Running the test script will also generate a Jacoco test report in the ```\build\reports\jacoco\test\html``` folder.

Postman was used for integration tests and those tests can be performed (while the app is running) through a tool called Newman:

```
npm install -g newman
newman run postman-collection.json -e postman-environment.json
```

Though, instead of installing that, one could simply import the collection and environment files into an instance of Postman and then run the tests there.

### Run

```
./gradlew bootRun
```

To actually give the application input to see it execute, this URL should be used after running it on the localhost:

```
http://localhost:8080/rovers
```
Where ```/rovers``` is the resource being requesting, giving the user the rovers supplied by the input by returning the resulting logic in JSON. The input string should be supplied as a parameter with the name ```input```. The string should have the necessary lines of input separated by a newline character, though there is some error handling if not.

I suggest either the use of Postman or SOAPUI to see results.

